package org.academiadecodigo.tailormoons.jogodogalo.panel;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.jogodogalo.Manager;
import org.academiadecodigo.tailormoons.jogodogalo.panel.cell.Cell;

public class Banner implements Panel {


    public static final int PADDING = 10;
    public static final int WIDTH = 400;
    public static final int HEIGHT = 100;
    private Rectangle banner;
    private static final String bannerImagePath = "resources/tictac.png";


    @Override
    public void setManager(Manager manager) {

    }

    @Override
    public void init() {

        Picture bannerImage = new Picture(PADDING, PADDING, bannerImagePath);
        bannerImage.draw();

        banner = new Rectangle(PADDING, PADDING, WIDTH, HEIGHT);
        banner.draw();
    }

    @Override
    public void receiveEvent(int x, int y) {

    }

    @Override
    public boolean isSelected(int x, int y) {
        return (x > PADDING && x < PADDING + Cell.SIZE * 4) &&
                (y > PADDING && y < PADDING + Cell.SIZE);
    }


}
