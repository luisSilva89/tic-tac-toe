package org.academiadecodigo.tailormoons.jogodogalo.panel;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.jogodogalo.Manager;

public class FeedPanel implements Panel {



    public static final int PADDING = 10;
    public static final int WIDTH = 400;
    public static final int HEIGHT = 100;

    private Rectangle panel;
    private Text messageToDisplay;
    private static final String panelImagePath = "resources/Panel.png";


    @Override
    public void setManager(Manager manager) {

    }

    @Override
    public void init() {

        Picture backgroundPanel = new Picture(PADDING, PADDING + Banner.HEIGHT + SidePanel.HEIGHT, panelImagePath);
        backgroundPanel.draw();

        panel = new Rectangle(PADDING, PADDING + Banner.HEIGHT + SidePanel.HEIGHT, WIDTH, HEIGHT);
        panel.draw();

    }

    @Override
    public void receiveEvent(int x, int y) {

    }

    @Override
    public boolean isSelected(int x, int y) {
        return (x > PADDING && x < PADDING + WIDTH) &&
                (y > PADDING + Banner.HEIGHT + SidePanel.HEIGHT && y < PADDING + Banner.HEIGHT + SidePanel.HEIGHT + HEIGHT);
    }


    public void displayMessage(String message) {

        if(messageToDisplay != null) {
            messageToDisplay.delete();
        }
        messageToDisplay = new Text(30, 430, message);
        messageToDisplay.draw();
    }

}
