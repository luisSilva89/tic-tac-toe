package org.academiadecodigo.tailormoons.jogodogalo.panel;

import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.tailormoons.jogodogalo.Manager;

public interface Panel {

    void setManager(Manager manager);

    void init();

    void receiveEvent(int x, int y);

    boolean isSelected(int x, int y);

}
