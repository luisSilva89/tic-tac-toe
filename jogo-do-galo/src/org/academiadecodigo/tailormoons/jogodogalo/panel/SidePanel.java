package org.academiadecodigo.tailormoons.jogodogalo.panel;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.jogodogalo.Manager;
import org.academiadecodigo.tailormoons.jogodogalo.panel.button.NewGameButton;
import org.academiadecodigo.tailormoons.jogodogalo.panel.button.NewRoundButton;


public class SidePanel implements Panel {


    public static final int PADDING = 10;
    public static final int WIDTH = 100;
    public static final int HEIGHT = 300;

    private NewGameButton newGameButton;
    private NewRoundButton newRoundButton;
    private Board board;

    private Rectangle bar;
    private static final String sidebarImagePath = "resources/Sidebar.png";


    public void setBoard(Board board) {
        this.board = board;
    }

    @Override
    public void setManager(Manager manager) {

    }

    @Override
    public void init() {

        Picture backgroundPanel = new Picture(PADDING, PADDING + Banner.HEIGHT, sidebarImagePath);
        backgroundPanel.draw();

        bar = new Rectangle(PADDING, PADDING + Banner.HEIGHT, WIDTH, HEIGHT);
        bar.draw();

        newGameButton = new NewGameButton();
        newGameButton.setBoard(board);
        newGameButton.init();

        newRoundButton = new NewRoundButton();
        newRoundButton.setBoard(board);
        newRoundButton.init();

    }

    @Override
    public void receiveEvent(int x, int y) {
        if((x > 15 && x < 100) && (y > 320 && y < 350)) {
            newRoundButton.action();
        } else if((x > 15 && x < 100) && (y > 360 && y < 395)) {
            newGameButton.action();
        }

    }

    @Override
    public boolean isSelected(int x, int y) {
        return (x > PADDING && x < PADDING + WIDTH) &&
                (y > PADDING + Banner.HEIGHT && y < PADDING + Banner.HEIGHT + HEIGHT);
    }


}
