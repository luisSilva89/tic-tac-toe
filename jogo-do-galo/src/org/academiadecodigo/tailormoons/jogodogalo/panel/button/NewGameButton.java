package org.academiadecodigo.tailormoons.jogodogalo.panel.button;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.jogodogalo.panel.Board;

public class NewGameButton {


    public static final int PADDING = 12;
    public static final int WIDTH = 100;
    public static final int HEIGHT = 50;

    private Board board;

    private static final String imagePath = "resources/NewGameButton.png";


    public void setBoard(Board board) {
        this.board = board;
    }

    public void init() {

        Picture newGameButton = new Picture(PADDING, 370, imagePath);
        newGameButton.draw();
    }


    public void action() {
        board.cleanBoard();
        board.resetCells();
        board.getGameManager().setRoundEnd(false);
        board.getGameManager().setPlayer1Score(0);
        board.getGameManager().setPlayer2Score(0);
        board.getGameManager().updateScoreBoard();
    }
}
