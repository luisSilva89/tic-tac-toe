package org.academiadecodigo.tailormoons.jogodogalo.panel.button;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.jogodogalo.panel.Board;

public class NewRoundButton {



    public static final int PADDING = 12;
    public static final int WIDTH = 100;
    public static final int HEIGHT = 50;

    private Board board;

    private static final String imagePath = "resources/NewRoundButton.png";


    public void init() {

        Picture startButton = new Picture(PADDING, 330, imagePath);
        startButton.draw();

    }

    public void setBoard(Board board) {
        this.board = board;
    }


    public void action() {

        board.cleanBoard();
        board.resetCells();
        board.getGameManager().updateScoreBoard();
        board.getGameManager().setTurn(1);
        board.getGameManager().setRoundEnd(false);

    }

}
