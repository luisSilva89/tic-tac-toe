package org.academiadecodigo.tailormoons.jogodogalo.panel;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.jogodogalo.GameManager;
import org.academiadecodigo.tailormoons.jogodogalo.Manager;
import org.academiadecodigo.tailormoons.jogodogalo.panel.cell.Cell;

import java.util.LinkedList;
import java.util.List;

public class Board implements Panel {

    public static final int PADDING = 10;
    public static final int COLS = 3;
    public static final int ROWS = 3;

    private List<Picture> pictures = new LinkedList<>();

    private GameManager gameManager;
    private Cell[][] cells;


    public GameManager getGameManager() {
        return gameManager;
    }

    public int checkCellValue(int col, int row) {
        return cells[col][row].getMemorizedPlay();
    }

    @Override
    public void setManager(Manager manager) {
        this.gameManager = (GameManager) manager;
    }

    @Override
    public void init() {
        cells = new Cell[ROWS][COLS];

        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {
                cells[row][col] = new Cell(row, col);
                cells[row][col].draw();
            }
        }
    }

    @Override
    public void receiveEvent(int x, int y) {

        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {

                Cell cell = cells[row][col];

                if (cell.isSelected(x, y)) {

                    if(gameManager.getPlayer().equals("Player 1")) {
                        String message = "Player 1 played a O on cell '" + (cell.getCOL() + 1) + " COL : " + (cell.getROW() + 1) + " ROW'";
                        gameManager.broadcast(message);
                    } else {
                        String message = "Player 2 played an X on cell '" + (cell.getCOL() + 1) + " COL : " + (cell.getROW() + 1) + " ROW'";
                        gameManager.broadcast(message);
                    }



                    if (!cell.isPlayed && !gameManager.isRoundEnd()) {

                        Picture picture = cell.drawPlay(gameManager.getTurn());
                        pictures.add(picture);
                        gameManager.nextTurn();
                        return;
                    }
                }
            }
        }

    }

    @Override
    public boolean isSelected(int x, int y) {

        int originX = Board.PADDING + SidePanel.WIDTH;
        int originY = Board.PADDING + Banner.HEIGHT;

        return (x > originX && x < originX + 3 * Cell.SIZE) &&
                (y > originY && y < originY + 3 * Cell.SIZE);
    }


    public void cleanBoard() {

        for(Picture picture: pictures) {
            picture.delete();
        }
        pictures.clear();

    }

    public void resetCells() {

        for (int row = 0; row < ROWS; row++) {
            for (int col = 0; col < COLS; col++) {

                cells[row][col].resetCell();
            }
        }
    }


}
