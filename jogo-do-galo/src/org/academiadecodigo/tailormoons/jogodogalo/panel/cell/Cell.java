package org.academiadecodigo.tailormoons.jogodogalo.panel.cell;

import org.academiadecodigo.simplegraphics.graphics.Rectangle;
import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.academiadecodigo.tailormoons.jogodogalo.panel.Banner;
import org.academiadecodigo.tailormoons.jogodogalo.panel.Board;
import org.academiadecodigo.tailormoons.jogodogalo.panel.SidePanel;


public class Cell {

    private Rectangle representation;

    public static final int SIZE = 100;
    public static final String BACKGROUND_PIC = "resources/Cell.png";
    public static final String CROSS = "resources/X.png";
    public static final String CIRCLE = "resources/O.png";


    private final int COL;
    private final int ROW;

    public boolean isPlayed;
    private static final int CROSS_PLAY = 1;
    private static final int CIRCLE_PLAY = -1;

    private int memorizedPlay;


    public Cell(int col, int row) {
        Picture backgroundPicture = new Picture(coordinateToPixel(col), coordinateToPixel(row), BACKGROUND_PIC);
        backgroundPicture.draw();
        representation = new Rectangle(coordinateToPixel(col), coordinateToPixel(row), SIZE, SIZE);
        this.COL = col;
        this.ROW = row;
    }

    public int getCOL() {
        return COL;
    }

    public int getROW() {
        return ROW;
    }

    public int getMemorizedPlay() {
        return memorizedPlay;
    }

    private int coordinateToPixel(int coordinate) {
        return Board.PADDING + Banner.HEIGHT + coordinate * SIZE;
    }

    public boolean isSelected(int x, int y) {

        int originX = Board.PADDING + SidePanel.WIDTH + COL * SIZE;
        int originY = Board.PADDING + Banner.HEIGHT + ROW * SIZE;

        return (x > originX && x < originX + SIZE) && (y > originY && y < originY + SIZE);
    }

    public Picture drawPlay(int turn) {

        Picture played;

        if (turn % 2 == 0) {

            played = new Picture(coordinateToPixel(COL), coordinateToPixel(ROW), CROSS);
            played.draw();
            isPlayed = true;
            memorizedPlay = CROSS_PLAY;

        } else {
            played = new Picture(coordinateToPixel(COL), coordinateToPixel(ROW), CIRCLE);
            played.draw();
            isPlayed = true;
            memorizedPlay = CIRCLE_PLAY;
        }

        return played;
    }

    public void draw() {
        representation.draw();
    }

    public void resetCell() {
        isPlayed = false;
        memorizedPlay = 0;

    }

}
