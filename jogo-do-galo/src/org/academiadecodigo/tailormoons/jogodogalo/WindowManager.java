package org.academiadecodigo.tailormoons.jogodogalo;


import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.tailormoons.jogodogalo.panel.*;

import java.util.LinkedList;
import java.util.List;


public class WindowManager {

    private List<Panel> panelList = new LinkedList<>();


    public WindowManager() {

        Panel banner = new Banner();
        banner.init();

        GameManager gameManager = new GameManager();

        Panel feedPanel = new FeedPanel();
        gameManager.setFeedPanel(feedPanel);
        feedPanel.init();

        Panel board = new Board();
        board.setManager(gameManager);
        gameManager.setBoard((Board) board);
        board.init();

        Panel sidePanel = new SidePanel();
        ((SidePanel) sidePanel).setBoard((Board) board);
        sidePanel.init();

        gameManager.initScoreBoard();

        panelList.add(banner);
        panelList.add(sidePanel);
        panelList.add(feedPanel);
        panelList.add(board);

    }


    public void receiveEvent(MouseEvent mouseEvent) {

        int x = (int) mouseEvent.getX();
        int y = (int) mouseEvent.getY();

        for (Panel panel : panelList) {
            if (panel.isSelected(x, y)) {
                panel.receiveEvent(x, y);
                return;
            }
        }
    }


}
