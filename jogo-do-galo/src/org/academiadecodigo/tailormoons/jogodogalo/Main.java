package org.academiadecodigo.tailormoons.jogodogalo;

import org.academiadecodigo.simplegraphics.graphics.Canvas;

public class Main {

    public static void main(String[] args) {

        Canvas canvas = Canvas.getInstance();
        canvas.getFrame().setSize(420, 557);

        WindowManager windowManager = new WindowManager();

        new MouseListener(windowManager);

    }
}
