package org.academiadecodigo.tailormoons.jogodogalo;

import org.academiadecodigo.simplegraphics.graphics.Text;
import org.academiadecodigo.tailormoons.jogodogalo.panel.FeedPanel;
import org.academiadecodigo.tailormoons.jogodogalo.panel.Board;
import org.academiadecodigo.tailormoons.jogodogalo.panel.Panel;

public class GameManager implements Manager {


    private Board board;
    private Panel feedPanel;

    private int turn = 1;
    private int player1Score;
    private int player2Score;
    private boolean roundEnd;

    private Text player1Points;
    private Text player2Points;


    public void setBoard(Board board) {
        this.board = board;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public void setPlayer1Score(int player1Score) {
        this.player1Score = player1Score;
    }

    public void setPlayer2Score(int player2Score) {
        this.player2Score = player2Score;
    }

    public boolean isRoundEnd() {
        return roundEnd;
    }

    public int getTurn() {
        getPlayer();
        return turn;
    }

    public void setFeedPanel(Panel feedPanel) {
        this.feedPanel = feedPanel;
    }

    public void setRoundEnd(boolean roundEnd) {
        this.roundEnd = roundEnd;
    }

    public void initScoreBoard() {
        Text player1 = new Text(25, 150, "Player 1 (O)");
        player1.draw();
        player1.grow(5, 10);

        Text player2 = new Text(25, 220, "Player 2 (X)");
        player2.draw();
        player2.grow(5, 10);

        player1Points = new Text(50, 185, Integer.toString(player1Score));
        player1Points.grow(5, 5);
        player1Points.draw();

        player2Points = new Text(50, 255, Integer.toString(player2Score));
        player2Points.grow(5, 5);
        player2Points.draw();

    }

    public void updateScoreBoard() {

        player1Points.setText(Integer.toString(player1Score));
        player2Points.setText(Integer.toString(player2Score));
    }


    public void nextTurn() {

        if (checkWinCondition()) {
            broadcast(getPlayer() + " WINS!!!!");
            roundEnd = true;
            System.out.println(roundEnd);
            System.out.println(getPlayer() + " WINS!!!");
            setGamePoint();
            return;
        }

        if (turn == 9) {
            broadcast("It's a TIE!!!");
            System.out.println("It's a TIE!!!");
        }
        turn++;
    }

    public void setGamePoint() {

        if (getPlayer().equals("Player 1")) {
            player1Score++;
        } else {
            player2Score++;
        }
    }

    public void broadcast(String message) {

        ((FeedPanel) feedPanel).displayMessage(message);
    }


    public String getPlayer() {

        if (turn % 2 != 0) {
            System.out.println("Player 1");
            return "Player 1";
        } else {
            System.out.println("Player 2");
            return "Player 2";
        }
    }


    public boolean checkWinCondition() {

        if (checkRowVictory()) {
            return true;
        } else if (checkColumnVictory()) {
            return true;
        } else return checkDiagonalVictory();
    }


    public boolean checkRowVictory() {

        int rowValue = 0;

        for (int i = 0; i < Board.ROWS; i++) {
            for (int j = 0; j < Board.COLS; j++) {
                rowValue += board.checkCellValue(j, i);
            }
            if (Math.abs(rowValue) == Board.COLS) {
                return true;
            }
            rowValue = 0;
        }
        return false;
    }


    public boolean checkColumnVictory() {

        int columnValue = 0;

        for (int i = 0; i < Board.COLS; i++) {
            for (int j = 0; j < Board.ROWS; j++) {
                columnValue += board.checkCellValue(i, j);
            }
            if (Math.abs(columnValue) == Board.ROWS) {
                return true;
            }
            columnValue = 0;
        }
        return false;
    }


    public boolean checkDiagonalVictory() {

        int leftDiagonal = 0;
        int rightDiagonal = 0;

        for (int i = 0; i < Board.ROWS; i++) {
            leftDiagonal = leftDiagonal + board.checkCellValue(i, i);
            rightDiagonal = rightDiagonal + board.checkCellValue(i, Board.ROWS - 1 - i);
        }
        return Math.abs(leftDiagonal) == Board.ROWS || Math.abs(rightDiagonal) == Board.ROWS;
    }

}

